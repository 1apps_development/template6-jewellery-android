package com.workdo.jewellery.base


import android.app.Application
import androidx.multidex.MultiDex


class BaseApplication : Application() {

    companion object {
        lateinit var app: BaseApplication



        fun getInstance(): BaseApplication {
            return app
        }
    }
    override fun onCreate() {
        super.onCreate()

        app = this
   /*     ViewPump.init(
            ViewPump.builder()
                .addInterceptor(
                    CalligraphyInterceptor(
                        CalligraphyConfig.Builder()
                            .setDefaultFontPath("font/poppins_regular.ttf")
                            .setFontAttrId(R.attr.fontPath)
                            .build()
                    )
                )
                .build()
        )*/
        MultiDex.install(this)

    }
}