package com.workdo.jewellery.adapter.variant

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.workdo.jewellery.R
import com.workdo.jewellery.databinding.CellSizeBinding
import com.workdo.jewellery.model.VariantDataItem
import com.workdo.jewellery.utils.ExtensionFunctions.capitalized

class VariantCollectionList(val itemList:ArrayList<VariantDataItem>, val context: Activity, private val variantItemClick: (VariantDataItem) -> Unit):RecyclerView.Adapter<VariantCollectionList.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = CellSizeBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bindItems(itemList[position],context,variantItemClick,position)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    inner class ViewHolder(private val itemBinding: CellSizeBinding) : RecyclerView.ViewHolder(itemBinding.root) {


        fun bindItems(data:VariantDataItem, context:Activity, variantItemClick: (VariantDataItem) -> Unit,position: Int)
        {

            if (data.isSelect == true) {
                itemBinding.card.background =
                    ResourcesCompat.getDrawable(itemView.context.resources, R.drawable.bg_green_8, null)
                itemBinding.tvsize.setTextColor(
                    AppCompatResources.getColorStateList(
                        itemView.context, R.color.white
                    )
                )
            } else {
                itemBinding.card.background = ResourcesCompat.getDrawable(context.resources, R.drawable.bg_green_border_8, null)
                itemBinding.tvsize.setTextColor(
                    AppCompatResources.getColorStateList(
                        context, R.color.darkgreen
                    )
                )
            }
            itemBinding.tvsize.text = data.name?.capitalized()
            itemView.setOnClickListener {
                for(i in 0 until itemList.size)
                {
                    itemList[i].isSelect=false
                }
                itemList[position].isSelect=true
                notifyDataSetChanged()
                variantItemClick(data)
            }
        }
    }
}