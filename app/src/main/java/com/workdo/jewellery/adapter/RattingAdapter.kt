package com.workdo.jewellery.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.jewellery.R
import com.workdo.jewellery.api.ApiClient
import com.workdo.jewellery.databinding.CellRattingBinding
import com.workdo.jewellery.model.ProductReview

class RattingAdapter(
    var context: Activity,
    private val rattingList: ArrayList<ProductReview>,
    private val onItemClick: (String, String) -> Unit
) : RecyclerView.Adapter<RattingAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =CellRattingBinding.inflate(LayoutInflater.from(parent.context),parent,false)

            LayoutInflater.from(parent.context)
            .inflate(R.layout.cell_ratting, parent, false)

        return ViewHolder(view)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bindItems(rattingList[position])

    }

    override fun getItemCount(): Int {
        return rattingList.size
    }

    class ViewHolder(val binding: CellRattingBinding) : RecyclerView.ViewHolder(binding.root) {


        fun bindItems(data :ProductReview)= with(binding)
        {
            Glide.with(itemView.context).load(ApiClient.ImageURL.BASE_URL.plus(data.productImage)).into(binding.ivProductImage)
           // Glide.with(itemView.context).load(ApiClient.ImageURL.BASE_URL.plus(data.userImage)).into(binding.ivUserImage)
            binding.tvTitle.text=data.title.toString()
            binding.tvSubTitle.text=data.review.toString()
           // binding.tvRattingDesc.text=data.review.toString()
            binding.tvUserName.text=data.userName.toString()

            binding.ivRatting.rating=data.rating?.toFloat()?:0.0f

        }

    }

    private fun onFilterClick(id: String, name: String) {
        onItemClick.invoke(id, name)
    }
}