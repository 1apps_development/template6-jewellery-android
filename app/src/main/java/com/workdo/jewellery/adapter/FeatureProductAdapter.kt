package com.workdo.jewellery.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import  com.workdo.jewellery.api.ApiClient
import com.workdo.jewellery.databinding.CellFeatureProductBinding
import  com.workdo.jewellery.model.FeaturedProductsSub
import  com.workdo.jewellery.utils.Constants
import  com.workdo.jewellery.utils.SharePreference
import  com.workdo.jewellery.utils.Utils

class FeatureProductAdapter (private val context: Activity,
                             private val productList: ArrayList<FeaturedProductsSub>,
                             private val itemClick: (Int, String) -> Unit) :RecyclerView.Adapter<FeatureProductAdapter.FeaturedViewHolder>(){


    inner class FeaturedViewHolder(private val binding: CellFeatureProductBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: FeaturedProductsSub,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {

            val currency = SharePreference.getStringPref(context, SharePreference.currency_name).toString()
            Glide.with(context).load(ApiClient.ImageURL.BASE_URL.plus(data.coverImagePath)).into(binding.ivProduct)
            binding.tvProductName.text = data.name.toString()
            binding.tvProductType.text=data.categoryName.toString()
        /*    binding.ivRatting.rating=data.averageRating?.toFloat()?:0f
            binding.tvRatting.text=data.averageRating.toString()


            binding.tvCurrency.text = currency*/



          /*  binding.btnAddToCart.setSafeOnClickListener {
                itemClick(position, Constants.CartClick)
            }*/
            binding.tvProductPrice.text = Utils.getPrice(data.finalPrice.toString())

            itemView.setOnClickListener {
                itemClick(position, Constants.ItemClick)
            }

        }


        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeaturedViewHolder {
        val view=CellFeatureProductBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return FeaturedViewHolder(view)
    }

    override fun onBindViewHolder(holder: FeaturedViewHolder, position: Int) {
        holder.bind(productList[position],context,position,itemClick)
    }

    override fun getItemCount(): Int {
        return productList.size
    }
}