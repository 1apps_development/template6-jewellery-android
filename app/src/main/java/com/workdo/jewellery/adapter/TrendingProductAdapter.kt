package com.workdo.jewellery.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.workdo.jewellery.R
import com.workdo.jewellery.api.ApiClient
import com.workdo.jewellery.databinding.CellProductshomeBinding

import com.workdo.jewellery.model.FeaturedProductsSub
import com.workdo.jewellery.utils.Constants
import com.workdo.jewellery.utils.ExtensionFunctions.hide
import com.workdo.jewellery.utils.ExtensionFunctions.show
import com.workdo.jewellery.utils.SharePreference
import com.workdo.jewellery.utils.Utils

class TrendingProductAdapter(
    private val context: Activity,
    private val providerList: ArrayList<FeaturedProductsSub>,
    private val itemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<TrendingProductAdapter.FeaturedViewHolder>() {
    var currency = SharePreference.getStringPref(context, SharePreference.currency_name).toString()

    inner class FeaturedViewHolder(private val binding: CellProductshomeBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            data: FeaturedProductsSub,
            context: Activity,
            position: Int,
            itemClick: (Int, String) -> Unit
        ) = with(binding)
        {

            Glide.with(context).load(ApiClient.ImageURL.BASE_URL.plus(data.coverImagePath))
                .into(binding.ivfeaturedProduct)
            binding.tvFeaturedProductName.text = data.name.toString()
            binding.tvFeaturedProductPrice.text =
                Utils.getPrice(data.finalPrice.toString())
            binding.tvcurrenytype.text = currency
            binding.tvTagName.text=data.tagApi.toString()
            //binding.tvTag.text = data.tagApi
            if (data.inWhishlist== true) {
                binding.ivWishList.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.ic_wishlist, null)
            } else {
                binding.ivWishList.background =
                    ResourcesCompat.getDrawable(context.resources, R.drawable.ic_diswishlist, null)
            }

            if(Utils.isLogin(context))
            {
                binding.ivWishList.show()
            }else
            {
                binding.ivWishList.hide()

            }
            binding.ivWishList.setOnClickListener {
                itemClick(position, Constants.FavClick)
            }
            binding.ivAddtocard.setOnClickListener {
                itemClick(position, Constants.CartClick)
            }
            itemView.setOnClickListener {
                itemClick(position, Constants.ItemClick)
            }


        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeaturedViewHolder {
        val view = CellProductshomeBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return FeaturedViewHolder(view)
    }

    override fun onBindViewHolder(holder: FeaturedViewHolder, position: Int) {
        holder.bind(providerList[position], context, position, itemClick)
    }

    override fun getItemCount(): Int {

        return providerList.size
    }
}