package com.workdo.jewellery.ui.activity

import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.util.Log
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.workdo.jewellery.R
import com.workdo.jewellery.adapter.ProductCategoryAdapter
import com.workdo.jewellery.api.ApiClient
import com.workdo.jewellery.base.BaseActivity
import com.workdo.jewellery.databinding.ActMenucatproductBinding
import com.workdo.jewellery.databinding.DlgConfirmBinding
import com.workdo.jewellery.model.FeaturedProductsSub
import com.workdo.jewellery.model.ProductListItem
import com.workdo.jewellery.remote.NetworkResponse
import com.workdo.jewellery.ui.authentication.ActWelCome
import com.workdo.jewellery.ui.option.ActFilter
import com.workdo.jewellery.utils.Constants
import com.workdo.jewellery.utils.ExtensionFunctions.hide
import com.workdo.jewellery.utils.ExtensionFunctions.show
import com.workdo.jewellery.utils.SharePreference
import com.workdo.jewellery.utils.Utils
import kotlinx.coroutines.launch
import java.lang.reflect.Type

class ActMenuCatProduct : BaseActivity() {
    private lateinit var _binding: ActMenucatproductBinding
    private var managerCatProduct: GridLayoutManager? = null
    private var categoriProductList = ArrayList<FeaturedProductsSub>()
    private lateinit var ProductCategoryAdapter: ProductCategoryAdapter
    internal var isLoadingCatPro = false
    internal var isLastPageCatPro = false
    private var currentPageCatPro = 1
    private var total_pagesCatPro: Int = 0
    var name = ""
    var subID = ""
    var mainID = ""
    var count = 1

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActMenucatproductBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        subID = intent.getStringExtra("sub_id").toString()
        mainID = intent.getStringExtra("main_id").toString()
        _binding.ivFilter.setOnClickListener { openActivity(ActFilter::class.java) }
        _binding.icBack.setOnClickListener { finish() }
        managerCatProduct =
            GridLayoutManager(this@ActMenuCatProduct, 2, GridLayoutManager.VERTICAL, false)
        nestedScrollViewPagination()
        productBannerApi()
    }

    //TODO product banner api
    private fun productBannerApi() {
        Utils.showLoadingProgress(this@ActMenuCatProduct)
        val productBanner = HashMap<String, String>()
        productBanner["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActMenuCatProduct)
                .productBanner(productBanner)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val bannersResponse = response.body.data?.themJson?.productsHeaderBanner
                    when (response.body.status) {
                        1 -> {
                            Glide.with(this@ActMenuCatProduct).load(
                                ApiClient.ImageURL.BASE_URL.plus(
                                    bannersResponse?.productsHeaderBanner
                                )
                            )
                                .into(_binding.ivProductBanner)
                            _binding.tvmess.text =
                                bannersResponse?.productsHeaderBannerTitleText.toString()

                        }
                        0 -> {

                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActMenuCatProduct)
                    } else {
                        Utils.errorAlert(
                            this@ActMenuCatProduct,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActMenuCatProduct,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActMenuCatProduct,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //TODO bestseller api
    private fun callBestseller() {
        Utils.showLoadingProgress(this@ActMenuCatProduct)
        val categoriesProduct = HashMap<String, String>()
        categoriesProduct["subcategory_id"] = "0"
        categoriesProduct["maincategory_id"] = mainID
        categoriesProduct["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            val response =
                if (SharePreference.getStringPref(this@ActMenuCatProduct, SharePreference.token)
                        ?.isEmpty() == true
                ) {
                    ApiClient.getClient(this@ActMenuCatProduct)
                        .setCategorysProductGuest(
                            currentPageCatPro.toString(),
                            categoriesProduct
                        )
                } else {
                    ApiClient.getClient(this@ActMenuCatProduct)
                        .setCategorysProduct(currentPageCatPro.toString(), categoriesProduct)
                }

            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val categoriProductResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                _binding.rvBestsellers.show()
                                _binding.vieww.hide()
                                currentPageCatPro =
                                    categoriProductResponse?.currentPage!!.toInt()
                                total_pagesCatPro =
                                    categoriProductResponse.lastPage!!.toInt()
                                categoriProductResponse.data?.let {
                                    categoriProductList.addAll(it)
                                }
                                if (currentPageCatPro >= total_pagesCatPro) {
                                    isLastPageCatPro = true
                                }
                                isLoadingCatPro = false

                            } else {
                                _binding.rvBestsellers.hide()
                                _binding.vieww.show()

                            }
                            ProductCategoryAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActMenuCatProduct,
                                categoriProductResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActMenuCatProduct,
                                categoriProductResponse?.data?.get(0)?.message.toString()
                            )
                            Utils.setInvalidToekn(this@ActMenuCatProduct)
                        }
                    }
                    if (categoriProductResponse?.to == null) {

                        _binding.tvFoundproduct.text =
                            getString(R.string.found).plus(" ").plus("0").plus(" ")
                                .plus(getString(R.string.product))
                    } else {
                        _binding.tvFoundproduct.text = getString(R.string.found).plus(" ").plus(
                            categoriProductResponse.total
                        ).plus(" ").plus(getString(R.string.product))
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActMenuCatProduct)
                    } else {
                        Utils.errorAlert(
                            this@ActMenuCatProduct,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActMenuCatProduct,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActMenuCatProduct,
                        getString(R.string.something_went_wrong)
                    )
                }
            }
        }
    }

    //TODO Adapter set Best seller
    private fun bestsellerAdapter(bestsellersList: ArrayList<FeaturedProductsSub>) {
        _binding.rvBestsellers.layoutManager = managerCatProduct
        ProductCategoryAdapter =
            ProductCategoryAdapter(this@ActMenuCatProduct, bestsellersList) { i: Int, s: String ->
                if (s == Constants.FavClick) {
                    if (SharePreference.getBooleanPref(
                            this@ActMenuCatProduct,
                            SharePreference.isLogin
                        )
                    ) {
                        Log.e("FavClick", "FavClick")
                        if (bestsellersList[i].inWhishlist == false) {
                            callWishlist(
                                "add", bestsellersList[i].id, "BestsellersList", i
                            )
                        } else {
                            callWishlist(
                                "remove",
                                bestsellersList[i].id,
                                "BestsellersList",
                                i
                            )
                        }
                    } else {
                        openActivity(ActWelCome::class.java)
                        finish()
                    }
                } else if (s == Constants.CartClick) {

                    if (!Utils.isLogin(this@ActMenuCatProduct)) {
                        guestUserAddToCart(bestsellersList[i], bestsellersList[i].id)
                    } else {
                        val addtocart = HashMap<String, String>()
                        addtocart["user_id"] =
                            SharePreference.getStringPref(
                                this@ActMenuCatProduct,
                                SharePreference.userId
                            )
                                .toString()
                        addtocart["product_id"] = bestsellersList[i].id.toString()
                        addtocart["variant_id"] = bestsellersList[i].defaultVariantId.toString()
                        addtocart["qty"] = "1"
                        addtocart["theme_id"] = getString(R.string.theme_id)

                        addtocartApi(
                            addtocart, bestsellersList[i].id,
                            bestsellersList[i].defaultVariantId, i
                        )
                    }
                } else if (s == Constants.ItemClick) {
                    val intent = Intent(this@ActMenuCatProduct, ActProductDetails::class.java)
                    intent.putExtra(Constants.ProductId, bestsellersList[i].id.toString())
                    startActivity(intent)
                }
            }
        _binding.rvBestsellers.adapter = ProductCategoryAdapter
    }

    //TODO guest user add to cart
    private fun guestUserAddToCart(data: FeaturedProductsSub, id: Int?) {
        val cartData = cartData(data)
        Log.e("cartData2", Gson().toJson(cartData))
        val cartListData = SharePreference.getStringPref(
            this@ActMenuCatProduct,
            SharePreference.GuestCartList
        )
        val type: Type = object : TypeToken<ArrayList<ProductListItem>>() {}.type
        var cartList = Gson().fromJson<ArrayList<ProductListItem>>(cartListData, type)
        Log.e("cartListBefore2", Gson().toJson(cartList))
        if (cartList == null) {
            val cartDataList = ArrayList<ProductListItem>()
            cartDataList.add(cartData)
            SharePreference.setStringPref(
                this@ActMenuCatProduct,
                SharePreference.GuestCartList,
                Gson().toJson(cartDataList)
            )
            SharePreference.setStringPref(
                this@ActMenuCatProduct,
                SharePreference.cartCount,
                cartDataList.size.toString()
            )

        } else if (isProductAlreadyAdded(cartList, data.id.toString())) {
            for (i in 0 until cartList.size) {
                if (cartList[i].productId?.toInt() == id) {
                    val position = cartList[i]
                    Log.e("SelectId", position.toString())
                    dlgAlreadyCart(data.message, data.id, data.defaultVariantId, i)
                }
            }
        } else {
            dlgConfirm(data.name.plus(" ").plus("add successfully."))

            cartList.add(cartData)
            SharePreference.setStringPref(
                this@ActMenuCatProduct,
                SharePreference.GuestCartList,
                Gson().toJson(cartList)
            )
            SharePreference.setStringPref(
                this@ActMenuCatProduct,
                SharePreference.cartCount,
                cartList.size.toString()
            )
        }
    }

    //TODO cart data set
    private fun cartData(data: FeaturedProductsSub): ProductListItem {
        return ProductListItem(
            data.coverImagePath.toString(),
            data.defaultVariantId.toString(),
            data.finalPrice.toString(),
            data.discountPrice.toString(),
            data.id.toString(),
            1,
            data.name.toString(),
            "",
            "",
            data.variantName.toString(),
            data.originalPrice.toString()
        )
    }

    private fun isProductAlreadyAdded(cartList: ArrayList<ProductListItem>, id: String): Boolean {
        return cartList.filter { it.productId == id.toString() }.size == 1
    }

    //TODO wishlist api
    private fun callWishlist(type: String, id: Int?, itemType: String, position: Int) {
        Utils.showLoadingProgress(this@ActMenuCatProduct)
        val wishlist = HashMap<String, String>()
        wishlist["user_id"] =
            SharePreference.getStringPref(this@ActMenuCatProduct, SharePreference.userId).toString()
        wishlist["product_id"] = id.toString()
        wishlist["wishlist_type"] = type
        wishlist["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActMenuCatProduct)
                .setWishlist(wishlist)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val wishlistResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if (type == "add") {
                                if (itemType == "BestsellersList") {
                                    categoriProductList[position].inWhishlist = true
                                    ProductCategoryAdapter.notifyItemChanged(position)
                                }
                            } else {
                                if (itemType == "BestsellersList") {
                                    categoriProductList[position].inWhishlist = false
                                    ProductCategoryAdapter.notifyItemChanged(position)
                                }
                            }
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActMenuCatProduct,
                                wishlistResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActMenuCatProduct,
                                wishlistResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                            this@ActMenuCatProduct.finish()
                            this@ActMenuCatProduct.finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActMenuCatProduct)
                    } else {
                        Utils.errorAlert(
                            this@ActMenuCatProduct,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActMenuCatProduct,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActMenuCatProduct,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    private fun nestedScrollViewPagination() {
        _binding.rvBestsellers.isNestedScrollingEnabled = false
        _binding.scrollView.viewTreeObserver?.addOnScrollChangedListener {
            val view = _binding.scrollView.getChildAt(_binding.scrollView.childCount - 1)
            val diff = view.bottom - (_binding.scrollView.height + _binding.scrollView.scrollY)
            if (diff == 0 && !isLoadingCatPro && !isLastPageCatPro) {
                currentPageCatPro++
                callBestseller()
            }
        }
    }

    //TODO add to cart api
    private fun addtocartApi(
        addtocart: HashMap<String, String>,
        id: Int?,
        defaultVariantId: Int?,
        i: Int?
    ) {
        Utils.showLoadingProgress(this@ActMenuCatProduct)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActMenuCatProduct)
                .addtocart(addtocart)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val addtoCartData = response.body.data
                    when (response.body.status) {
                        1 -> {
                            dlgConfirm(response.body.data?.message)
                            SharePreference.setStringPref(
                                this@ActMenuCatProduct,
                                SharePreference.cartCount,
                                addtoCartData?.count.toString()
                            )
                        }
                        0 -> {
                            dlgAlreadyCart(response.body.data?.message, id, defaultVariantId, i)
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActMenuCatProduct,
                                addtoCartData?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                            this@ActMenuCatProduct.finish()
                            this@ActMenuCatProduct.finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActMenuCatProduct)
                    } else {
                        Utils.errorAlert(
                            this@ActMenuCatProduct,
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActMenuCatProduct,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActMenuCatProduct,
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //TODO checkout or continue shopping dialog
    private fun dlgConfirm(message: String?) {
        val confirmDialogBinding = DlgConfirmBinding.inflate(layoutInflater)
        val dialog = Dialog(this@ActMenuCatProduct)
        dialog.setContentView(confirmDialogBinding.root)
        confirmDialogBinding.tvProductDesc.text = message
        confirmDialogBinding.tvCheckOut.setOnClickListener {
            dialog.dismiss()
            openActivity(ActShoppingCart::class.java)
        }
        confirmDialogBinding.tvContinueShopping.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    //TODO item alreay cart dialog
    private fun dlgAlreadyCart(message: String?, id: Int?, defaultVariantId: Int?, i: Int?) {
        val builder = AlertDialog.Builder(this@ActMenuCatProduct)
        builder.setTitle(R.string.app_name)
        builder.setMessage(R.string.product_already_again)
        builder.setPositiveButton(getString(R.string.yes)) { dialogInterface, which ->
            val cartqty = java.util.HashMap<String, String>()

            if (Utils.isLogin(this@ActMenuCatProduct)) {
                count = 1
                cartqty["product_id"] = id.toString()
                cartqty["user_id"] =
                    SharePreference.getStringPref(
                        this@ActMenuCatProduct,
                        SharePreference.userId
                    ).toString()
                cartqty["variant_id"] = defaultVariantId.toString()
                cartqty["quantity_type"] = "increase"
                cartqty["theme_id"]=getString(R.string.theme_id)
                cartQtyApi(cartqty, "increase")

            } else {
                val cartListData = SharePreference.getStringPref(
                    this@ActMenuCatProduct,
                    SharePreference.GuestCartList
                )
                val type: Type = object : TypeToken<java.util.ArrayList<ProductListItem>>() {}.type
                var cartList =
                    Gson().fromJson<java.util.ArrayList<ProductListItem>>(cartListData, type)
                var count = cartList[i!!].qty?.toInt()!!
                Log.e("Count", count.toString())
                cartList[i].qty = count.plus(1)

                manageOfflineData(cartList)
            }
        }
        builder.setNegativeButton(getString(R.string.no)) { dialogInterface, which ->
            builder.setCancelable(true)
        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    //TODO cart qty api
    private fun cartQtyApi(
        cartqty: java.util.HashMap<String, String>,
        type: String
    ) {
        Utils.showLoadingProgress(this@ActMenuCatProduct)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(this@ActMenuCatProduct)
                .cartQty(cartqty)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val cartQtyResponse = response.body.data
                    SharePreference.setStringPref(
                        this@ActMenuCatProduct,
                        SharePreference.cartCount,
                        response.body.count.toString()
                    )

                    when (response.body.status) {
                        1 -> {
                            Utils.dismissLoadingProgress()
                        }
                        0 -> {
                            Utils.errorAlert(
                                this@ActMenuCatProduct,
                                cartQtyResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                this@ActMenuCatProduct,
                                cartQtyResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(this@ActMenuCatProduct)
                    } else {
                        Utils.errorAlert(
                            this@ActMenuCatProduct, response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        this@ActMenuCatProduct,
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(this@ActMenuCatProduct, "Something went wrong")
                }
            }
        }
    }

    //TODO guest user offline data set
    private fun manageOfflineData(cartItemList: java.util.ArrayList<ProductListItem>) {
        SharePreference.setStringPref(
            this@ActMenuCatProduct,
            SharePreference.GuestCartList,
            Gson().toJson(cartItemList)
        )
        SharePreference.setStringPref(
            this@ActMenuCatProduct,
            SharePreference.cartCount,
            cartItemList.size.toString()
        )
    }

    override fun onResume() {
        super.onResume()
        currentPageCatPro = 1
        categoriProductList.clear()
        bestsellerAdapter(categoriProductList)
        callBestseller()
    }
}