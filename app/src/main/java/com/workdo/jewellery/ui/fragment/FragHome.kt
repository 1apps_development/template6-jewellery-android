package com.workdo.jewellery.ui.fragment

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.workdo.jewellery.R
import com.workdo.jewellery.adapter.AllCategoryAdapter
import com.workdo.jewellery.adapter.BestsellerAdapter
import com.workdo.jewellery.adapter.FeatureProductAdapter
import com.workdo.jewellery.adapter.TrendingProductAdapter
import com.workdo.jewellery.api.ApiClient
import com.workdo.jewellery.base.BaseFragment
import com.workdo.jewellery.databinding.DlgConfirmBinding
import com.workdo.jewellery.databinding.FragHomeBinding
import com.workdo.jewellery.model.FeaturedProductsSub
import com.workdo.jewellery.model.HomeCategoriesItem
import com.workdo.jewellery.model.ProductListItem
import com.workdo.jewellery.remote.NetworkResponse
import com.workdo.jewellery.ui.activity.ActCategoryProduct
import com.workdo.jewellery.ui.activity.ActProductDetails
import com.workdo.jewellery.ui.activity.ActShoppingCart
import com.workdo.jewellery.ui.activity.MainActivity
import com.workdo.jewellery.ui.authentication.ActWelCome
import com.workdo.jewellery.ui.option.ActMenu
import com.workdo.jewellery.ui.option.ActSearch
import com.workdo.jewellery.utils.Constants
import com.workdo.jewellery.utils.ExtensionFunctions.hide
import com.workdo.jewellery.utils.ExtensionFunctions.show
import com.workdo.jewellery.utils.PaginationScrollListener
import com.workdo.jewellery.utils.SharePreference
import com.workdo.jewellery.utils.Utils
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import java.lang.reflect.Type

class FragHome : BaseFragment<FragHomeBinding>() {
    private lateinit var _binding: FragHomeBinding
    private var managerAllCategories: GridLayoutManager? = null
    private var homeCategoriesList = ArrayList<HomeCategoriesItem>()
    private lateinit var allCategoriesAdapter: AllCategoryAdapter
    internal var isLoadingCategorirs = false
    internal var isLastPageCategorirs = false
    private var currentPageCategorirs = 1
    private var total_pagesCategorirs: Int = 0

    private var managerBestsellersSecond: GridLayoutManager? = null
    private var managerBestsellers: GridLayoutManager? = null
    private var bestsellersList = ArrayList<FeaturedProductsSub>()
    private lateinit var bestSellersAdapter: BestsellerAdapter
    private lateinit var trendingProductAdapter: TrendingProductAdapter
    private lateinit var randomProductAdapter: FeatureProductAdapter


    private var trendingProductList = ArrayList<FeaturedProductsSub>()

    internal var isLoadingBestSeller = false
    internal var isLastPageBestSeller = false
    private var currentPageBestSeller = 1
    private var totalPagesBestSeller: Int = 0

    internal var isLoadingTrending = false
    internal var isLastPageTrending = false
    private var currentPageTrending = 1
    private var totalPagesTrending: Int = 0
    var searched = ""
    private val randomProductList = ArrayList<FeaturedProductsSub>()

    var count = 1
    override fun initView(view: View) {
        init()
        setupRandomProductAdapter()
    }

    override fun getBinding(): FragHomeBinding {
        _binding = FragHomeBinding.inflate(layoutInflater)
        return _binding
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun init() {

        _binding.btnCheckMore.setOnClickListener {
            val intent = Intent(requireActivity(), MainActivity::class.java)
            intent.putExtra("pos", "2")
            startActivity(intent)
        }
        _binding.clSearch.setOnClickListener {
            startActivity(
                Intent(requireActivity(), ActSearch::class.java).putExtra(
                    "Searched",
                    searched
                )
            )
        }
        _binding.edSearch.setOnClickListener {
            startActivity(
                Intent(requireActivity(), ActSearch::class.java).putExtra(
                    "Searched",
                    searched
                )
            )
        }
        _binding.ivCart.setOnClickListener { openActivity(ActShoppingCart::class.java) }
        _binding.ivMenu.setOnClickListener { openActivity(ActMenu::class.java) }
        paginationCategories()
        paginationBestSeller()
        paginationTrendingProducts()
        managerAllCategories =
            GridLayoutManager(requireActivity(), 1, GridLayoutManager.VERTICAL, false)
        managerBestsellers =
            GridLayoutManager(requireActivity(), 1, GridLayoutManager.HORIZONTAL, false)
        managerBestsellersSecond =
            GridLayoutManager(requireActivity(), 1, GridLayoutManager.HORIZONTAL, false)

        if (SharePreference.getStringPref(requireActivity(), SharePreference.cartCount)
                .isNullOrEmpty()
        ) {
            SharePreference.setStringPref(requireActivity(), SharePreference.cartCount, "0")
        }

    }


    private val handler = Handler(Looper.getMainLooper())
    private val scroll = 2000
    private val autoScroll = object : Runnable {
        var count = 0
        override fun run() {
            Log.e("count", count.toString())
            if (count == (_binding.rvFeature.adapter?.itemCount ?: 0)) {
                count = -1

            }
            if (count < (_binding.rvFeature.adapter?.itemCount ?: -1)) {
                _binding.rvFeature.smoothScrollToPosition(++count)
                handler.postDelayed(this, scroll.toLong())
            }
        }
    }

    private fun callRandomProduct() {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            val request = HashMap<String, String>()
            request["theme_id"] = resources.getString(R.string.theme_id)
            when (val response = ApiClient.getClient(requireActivity()).randomProduct(request)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    when (response.body.status) {
                        1 -> {
                            Utils.dismissLoadingProgress()

                            val data = response.body.data
                            if (data?.size == 0) {
                                _binding.rvFeature.hide()
                            } else {
                                randomProductList.clear()
                                data?.let { randomProductList.addAll(it) }
                                _binding.rvFeature.show()
                                randomProductAdapter.notifyDataSetChanged()
                                handler.removeCallbacks(autoScroll)
                                handler.postDelayed(autoScroll, scroll.toLong())
                            }

                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                response.body.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                response.body.message.toString()
                            )
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }

    private fun setupRandomProductAdapter() {
        _binding.rvFeature.layoutManager =
            GridLayoutManager(requireActivity(), 1, GridLayoutManager.HORIZONTAL, false)
        randomProductAdapter =
            FeatureProductAdapter(requireActivity(), randomProductList) { i: Int, s: String ->

                if (s == Constants.ItemClick) {
                    val intent = Intent(requireActivity(), ActProductDetails::class.java)
                    intent.putExtra(Constants.ProductId, randomProductList[i].id.toString())
                    startActivity(intent)
                }

            }

        _binding.rvFeature.adapter = randomProductAdapter
    }

    //TODO Currency Api calling
    private fun callCurrencyApi() {
        Utils.showLoadingProgress(requireActivity())
        val currencyMap = HashMap<String, String>()
        currencyMap["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setcurrency(currencyMap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val currencyResponse = response.body.data
                    when (response.body.status) {
                        1 -> {

                            SharePreference.setStringPref(
                                requireActivity(),
                                SharePreference.currency,
                                currencyResponse?.currency.toString()
                            )
                            SharePreference.setStringPref(
                                requireActivity(),
                                SharePreference.currency_name,
                                currencyResponse?.currency_name.toString()
                            )
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                currencyResponse?.message.toString()
                            )
                        }
                        9 -> {
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                            // openActivity(ActWelCome::class.java)
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(requireActivity(), response.body.message.toString())
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()

                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
                else -> {}
            }
        }
    }

    //TODO Header content api calling
    private fun callHeaderContentApi() {
        Utils.showLoadingProgress(requireActivity())
        val theme = HashMap<String, String>()
        theme["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setLandingPage(theme)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val headerContenResponse = response.body.data
                    when (response.body.status) {
                        1 -> {

                            /* Glide.with(requireActivity())
                                 .load(ApiClient.ImageURL.BASE_URL.plus(headerContenResponse?.themJson?.homepageHeader?.homepageHeaderBgImg!!.get(0).toString()))
                                 .into(_binding.ivProduct)*/

                            _binding.tvJewellery.text =
                                headerContenResponse?.themJson?.homepageHeader?.homepageHeaderTitle
                            _binding.tvLorem.text =
                                headerContenResponse?.themJson?.homepageHeader?.homepageHeaderSubText
                            _binding.btnCheckMore.text =
                                headerContenResponse?.themJson?.homepageHeader?.homepageHeaderBtn

                            /* Glide.with(requireActivity())
                                 .load(ApiClient.ImageURL.BASE_URL.plus(headerContenResponse?.themJson?.homepagePromotions1?.homepagePromotionsPromotionIcon))
                                 .into(_binding.ivGift)
                             _binding.tvFinishedProducts.text =
                                 headerContenResponse?.themJson?.homepagePromotions1?.homepagePromotionsTitle
                             _binding.tvGiftWrapping.text =
                                 headerContenResponse?.themJson?.homepagePromotions1?.homepagePromotionsSubText

                             Glide.with(requireActivity())
                                 .load(ApiClient.ImageURL.BASE_URL.plus(headerContenResponse?.themJson?.homepagePromotions2?.homepagePromotionsPromotionIcon))
                                 .into(_binding.ivFreeShipping)
                             _binding.tvFreeShipping.text =
                                 headerContenResponse?.themJson?.homepagePromotions2?.homepagePromotionsTitle
                             _binding.tvFreeShippingOrder.text =
                                 headerContenResponse?.themJson?.homepagePromotions2?.homepagePromotionsSubText
 */
                            _binding.tvJewellery1.text =
                                headerContenResponse?.themJson?.homepageBestProduct?.homepageBestProductTitle
                            _binding.tvLorem1.text =
                                headerContenResponse?.themJson?.homepageBestProduct?.homepageBestProductSubText
                            _binding.tvJewellery2.text =
                                headerContenResponse?.themJson?.homepageBanner?.homepageBannerTitle
                            _binding.tvLorem2.text =
                                headerContenResponse?.themJson?.homepageBanner?.homepageBannerSubText


                            Glide.with(requireActivity())
                                .load(
                                    ApiClient.ImageURL.BASE_URL.plus(
                                        headerContenResponse?.themJson?.homepageProducts?.homepageProductsBgImg!!.get(
                                            0
                                        ).toString()
                                    )
                                )
                                .into(_binding.ivHome2)

                            Glide.with(requireActivity())
                                .load(
                                    ApiClient.ImageURL.BASE_URL.plus(
                                        headerContenResponse?.themJson?.homepageProducts?.homepageProductsBgImg!!.get(
                                            1
                                        ).toString()
                                    )
                                )
                                .into(_binding.ivHome1)

                            _binding.tvgetOff.text =
                                headerContenResponse?.themJson?.homepageNewsletter?.homepageNewsletterTitle
                            _binding.tvLorem3.text =
                                headerContenResponse?.themJson?.homepageNewsletter?.homepageNewsletterSubText
                            _binding.tvEmail.text =
                                headerContenResponse?.themJson?.homepageNewsletter?.homepageNewsletterDescription

                            /* if (response.body.data?.loyalitySection == "on") {

                             } else {

                             }*/
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                headerContenResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                headerContenResponse?.message.toString()
                            )
                            startActivity(Intent(requireActivity(), ActWelCome::class.java))
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
                else -> {}
            }
        }
    }

    //TODO adapter set Categoris
    private fun categoriesAdapter(homeCategoriesList: ArrayList<HomeCategoriesItem>) {
        _binding.rvAllCategories.layoutManager = managerAllCategories
        allCategoriesAdapter =
            AllCategoryAdapter(requireActivity(), homeCategoriesList) { i: Int, s: String ->
                if (s == Constants.ItemClick) {
                    startActivity(
                        Intent(
                            requireActivity(),
                            ActCategoryProduct::class.java
                        ).putExtra("maincategory_id", homeCategoriesList[i].categoryId.toString())
                    )
                }
            }
        _binding.rvAllCategories.adapter = allCategoriesAdapter
    }

    //TODO All Categories pagination
    private fun paginationCategories() {

        val paginationListener = object : PaginationScrollListener(managerAllCategories) {
            override fun isLastPage(): Boolean {
                return isLastPageCategorirs
            }

            override fun isLoading(): Boolean {
                return isLoadingCategorirs
            }

            override fun loadMoreItems() {
                isLoadingCategorirs = true
                currentPageCategorirs++
                callCategories()
            }
        }
        _binding.rvAllCategories.addOnScrollListener(paginationListener)
    }


    //TODO  Categories api callig
    private fun callCategories() {
        Utils.showLoadingProgress(requireActivity())
        val categorieshashmap = HashMap<String, String>()
        categorieshashmap["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setHomeCategorys(currentPageCategorirs.toString(), categorieshashmap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val categoriesResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {

                                _binding.rvAllCategories.show()
                                this@FragHome.currentPageCategorirs =
                                    categoriesResponse?.currentPage!!.toInt()
                                this@FragHome.total_pagesCategorirs =
                                    categoriesResponse.lastPage!!.toInt()
                                categoriesResponse.data?.let {
                                    homeCategoriesList.addAll(it)
                                }
                                if (currentPageCategorirs >= total_pagesCategorirs) {
                                    isLastPageCategorirs = true
                                }
                                isLoadingCategorirs = false
                            } else {
                                _binding.rvAllCategories.hide()

                            }
                            allCategoriesAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                categoriesResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                categoriesResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }


    private fun callBestseller() {
        Utils.showLoadingProgress(requireActivity())
        val bestseller = HashMap<String, String>()
        bestseller["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {

            val response =
                if (SharePreference.getStringPref(requireActivity(), SharePreference.token)
                        ?.isEmpty() == true
                ) {
                    ApiClient.getClient(requireActivity())
                        .setBestSellerGuest(currentPageBestSeller.toString(), bestseller)
                } else {
                    ApiClient.getClient(requireActivity())
                        .setBestSeller(currentPageBestSeller.toString(), bestseller)
                }

            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val bestsellerResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                if (Utils.isLogin(requireActivity())) {
                                    SharePreference.setStringPref(
                                        requireActivity(),
                                        SharePreference.cartCount,
                                        response.body.count.toString()
                                    )
                                    _binding.tvCount.text = response.body.count.toString()
                                }



                                _binding.rvBestsellers.show()

                                currentPageBestSeller = bestsellerResponse?.currentPage!!.toInt()
                                totalPagesBestSeller = bestsellerResponse.lastPage!!.toInt()
                                bestsellerResponse.data?.let {
                                    bestsellersList.addAll(it)
                                }


                                if (currentPageBestSeller >= totalPagesBestSeller) {
                                    isLastPageBestSeller = true
                                }
                                isLoadingBestSeller = false
                            } else {
                                _binding.rvBestsellers.hide()
                            }
                            bestSellersAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                bestsellerResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                bestsellerResponse?.data?.get(0)?.message.toString()
                            )
                            Utils.setInvalidToekn(requireActivity())
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        getString(R.string.something_went_wrong)
                    )
                }
                else -> {}
            }
        }
    }

    //Adapter set Best seller
    private fun bestsellerAdapter(bestsellersList: ArrayList<FeaturedProductsSub>) {
        _binding.rvBestsellers.layoutManager = managerBestsellers
        bestSellersAdapter =
            BestsellerAdapter(requireActivity(), bestsellersList) { i: Int, s: String ->
                if (s == Constants.FavClick) {
                    if (SharePreference.getBooleanPref(
                            requireActivity(),
                            SharePreference.isLogin
                        )
                    ) {
                        Log.e("FavClick", "FavClick")
                        if (bestsellersList[i].inWhishlist == false) {
                            callWishlist(
                                "add", bestsellersList[i].id, "BestsellersList", i
                            )
                        } else {
                            callWishlist(
                                "remove",
                                bestsellersList[i].id,
                                "BestsellersList",
                                i
                            )
                        }
                    } else {
                        openActivity(ActWelCome::class.java)
                        activity?.finish()
                    }
                } else if (s == Constants.CartClick) {
                    val data = bestsellersList[i]
                    if (!Utils.isLogin(requireActivity())) {
                        guestUserAddToCart(data, bestsellersList[i].id)
                    } else {
                        val addtocart = HashMap<String, String>()
                        addtocart["user_id"] =
                            SharePreference.getStringPref(requireActivity(), SharePreference.userId)
                                .toString()
                        addtocart["product_id"] = bestsellersList[i].id.toString()
                        addtocart["variant_id"] = bestsellersList[i].defaultVariantId.toString()
                        addtocart["qty"] = "1"
                        addtocart["theme_id"] = getString(R.string.theme_id)
                        addtocartApi(
                            addtocart,
                            bestsellersList[i].id,
                            bestsellersList[i].defaultVariantId, i
                        )
                    }
                } else if (s == Constants.ItemClick) {
                    val intent = Intent(requireActivity(), ActProductDetails::class.java)
                    intent.putExtra(Constants.ProductId, bestsellersList[i].id.toString())
                    startActivity(intent)
                }
            }
        _binding.rvBestsellers.adapter = bestSellersAdapter
    }

    private fun paginationBestSeller() {
        val paginationListener = object : PaginationScrollListener(managerBestsellers) {
            override fun isLastPage(): Boolean {
                return isLastPageBestSeller
            }

            override fun isLoading(): Boolean {
                return isLoadingBestSeller
            }

            override fun loadMoreItems() {
                isLoadingBestSeller = true
                currentPageBestSeller++
                callBestseller()
            }
        }
        _binding.rvBestsellers.addOnScrollListener(paginationListener)
    }

    private fun callTrendingProducts() {
        Utils.showLoadingProgress(requireActivity())
        val request = HashMap<String, String>()
        request["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {

            val response =
                if (SharePreference.getStringPref(requireActivity(), SharePreference.token)
                        ?.isEmpty() == true
                ) {
                    ApiClient.getClient(requireActivity())
                        .setTrendingProductGuest(currentPageTrending.toString(), request)
                } else {
                    ApiClient.getClient(requireActivity())
                        .setTrendingProduct(currentPageTrending.toString(), request)
                }

            when (response) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val trendingResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {


                                _binding.rvTrendingProduct.show()

                                currentPageTrending =
                                    trendingResponse?.currentPage!!.toInt()
                                totalPagesTrending = trendingResponse.lastPage!!.toInt()
                                trendingResponse.data?.let {
                                    trendingProductList.addAll(it)
                                }

                                if (currentPageTrending >= currentPageTrending) {
                                    isLastPageTrending = true
                                }
                                isLoadingTrending = false
                            } else {
                                _binding.rvTrendingProduct.hide()
                            }
                            trendingProductAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                trendingResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                trendingResponse?.data?.get(0)?.message.toString()
                            )
                            Utils.setInvalidToekn(requireActivity())
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        getString(R.string.something_went_wrong)
                    )
                }
                else -> {}
            }
        }
    }

    //Adapter set Best seller
    private fun trendingProductAdapter(bestsellersList: ArrayList<FeaturedProductsSub>) {
        _binding.rvTrendingProduct.layoutManager = managerBestsellersSecond
        trendingProductAdapter =
            TrendingProductAdapter(requireActivity(), bestsellersList) { i: Int, s: String ->
                if (s == Constants.FavClick) {
                    if (SharePreference.getBooleanPref(
                            requireActivity(),
                            SharePreference.isLogin
                        )
                    ) {
                        Log.e("FavClick", "FavClick")
                        if (bestsellersList[i].inWhishlist == false) {
                            callWishlist(
                                "add", bestsellersList[i].id, "TrendingList", i
                            )
                        } else {
                            callWishlist(
                                "remove",
                                bestsellersList[i].id,
                                "TrendingList",
                                i
                            )
                        }
                    } else {
                        openActivity(ActWelCome::class.java)
                        activity?.finish()
                    }
                } else if (s == Constants.CartClick) {
                    val data = bestsellersList[i]
                    if (!Utils.isLogin(requireActivity())) {
                        guestUserAddToCart(data, bestsellersList[i].id)
                    } else {
                        val addtocart = HashMap<String, String>()
                        addtocart["user_id"] =
                            SharePreference.getStringPref(requireActivity(), SharePreference.userId)
                                .toString()
                        addtocart["product_id"] = bestsellersList[i].id.toString()
                        addtocart["variant_id"] = bestsellersList[i].defaultVariantId.toString()
                        addtocart["qty"] = "1"
                        addtocart["theme_id"] = getString(R.string.theme_id)
                        addtocartApi(
                            addtocart,
                            bestsellersList[i].id,
                            bestsellersList[i].defaultVariantId, i
                        )
                    }
                } else if (s == Constants.ItemClick) {
                    val intent = Intent(requireActivity(), ActProductDetails::class.java)
                    intent.putExtra(Constants.ProductId, bestsellersList[i].id.toString())
                    startActivity(intent)
                }
            }
        _binding.rvTrendingProduct.adapter = trendingProductAdapter
    }

    private fun paginationTrendingProducts() {
        val paginationListener = object : PaginationScrollListener(managerBestsellersSecond) {
            override fun isLastPage(): Boolean {
                return isLastPageTrending
            }

            override fun isLoading(): Boolean {
                return isLoadingTrending
            }

            override fun loadMoreItems() {
                isLoadingTrending = true
                currentPageTrending++
                callTrendingProducts()
            }
        }
        _binding.rvTrendingProduct.addOnScrollListener(paginationListener)
    }


    private fun callWishlist(type: String, id: Int?, itemType: String, position: Int) {
        Utils.showLoadingProgress(requireActivity())
        val wishlist = HashMap<String, String>()
        wishlist["user_id"] =
            SharePreference.getStringPref(requireActivity(), SharePreference.userId).toString()
        wishlist["product_id"] = id.toString()
        wishlist["wishlist_type"] = type
        wishlist["theme_id"] = getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setWishlist(wishlist)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val wishlistResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if (type == "add") {
                                when (itemType) {
                                    "CategoriesProduct" -> {
                                        //featuredProductsSubList[position].inWhishlist = true
                                        //  categoriesProductAdapter.notifyItemChanged(position)
                                    }
                                    "BestsellersList" -> {
                                        bestsellersList[position].inWhishlist = true
                                        bestSellersAdapter.notifyItemChanged(position)
                                    }
                                    "TrendingList" -> {
                                        trendingProductList[position].inWhishlist = true
                                        trendingProductAdapter.notifyItemChanged(position)
                                    }
                                }
                            } else {
                                when (itemType) {
                                    "CategoriesProduct" -> {
                                        // featuredProductsSubList[position].inWhishlist = false
                                        // categoriesProductAdapter.notifyItemChanged(position)
                                    }
                                    "BestsellersList" -> {
                                        bestsellersList[position].inWhishlist = false
                                        bestSellersAdapter.notifyItemChanged(position)
                                    }
                                    "TrendingList" -> {
                                        trendingProductList[position].inWhishlist = false
                                        trendingProductAdapter.notifyItemChanged(position)
                                    }
                                }
                            }
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                wishlistResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                wishlistResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }

                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
                else -> {}
            }
        }
    }


    private fun guestUserAddToCart(data: FeaturedProductsSub, id: Int?) {
        Log.e("Position", id.toString())
        val cartData = cartData(data)
        Log.e("cartData2", Gson().toJson(cartData))

        val cartListData = SharePreference.getStringPref(
            requireActivity(),
            SharePreference.GuestCartList
        )
        val type: Type = object : TypeToken<ArrayList<ProductListItem>>() {}.type

        var cartList =
            Gson().fromJson<ArrayList<ProductListItem>>(cartListData, type)
        Log.e("cartListBefore2", Gson().toJson(cartList))

        if (cartList == null) {
            //  dlgConfirm(data.name+" "+R.string.addsuccess)
            val cartDataList = ArrayList<ProductListItem>()
            cartDataList.add(cartData)

            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.GuestCartList,
                Gson().toJson(cartDataList)
            )
            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.cartCount,
                cartDataList.size.toString()
            )
            _binding.tvCount.text = cartDataList.size.toString()
        } else if (isProductAlreadyAdded(cartList, data.id.toString())) {
            // Utils.errorAlert(requireActivity(), "item already added into cart")
            for (i in 0 until cartList.size) {
                if (cartList[i].productId?.toInt() == id) {
                    val position = cartList[i]
                    Log.e("SelectId", position.toString())
                    dlgAlreadyCart(data.message, data.id, data.defaultVariantId, i)
                }
            }
        } else {
            dlgConfirm(data.name.plus(" ").plus("add successfully."))
            cartList.add(cartData)
            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.GuestCartList,
                Gson().toJson(cartList)
            )
            SharePreference.setStringPref(
                requireActivity(),
                SharePreference.cartCount,
                cartList.size.toString()
            )
            _binding.tvCount.text = cartList.size.toString()
        }
    }

    private fun isProductAlreadyAdded(cartList: ArrayList<ProductListItem>, id: String): Boolean {
        return cartList.filter { it.productId == id.toString() }.size == 1
    }

    private fun cartData(data: FeaturedProductsSub): ProductListItem {
        return ProductListItem(
            data.coverImagePath.toString(),
            data.defaultVariantId.toString(),
            data.finalPrice.toString(),
            data.discountPrice.toString(),
            data.id.toString(),
            1,
            data.name.toString(),
            "",
            "",
            data.variantName.toString(),
            data.originalPrice.toString()
        )
    }

    private fun addtocartApi(
        addtocart: HashMap<String, String>,
        id: Int?,
        defaultVariantId: Int?,
        i: Int?,
    ) {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .addtocart(addtocart)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val addtocart = response.body.data
                    when (response.body.status) {
                        1 -> {
                            dlgConfirm(response.body.data?.message)
                            SharePreference.setStringPref(
                                requireActivity(),
                                SharePreference.cartCount,
                                addtocart?.count.toString()
                            )
                            setCount(addtocart?.count)
                        }
                        0 -> {
                            dlgAlreadyCart(response.body.data?.message, id, defaultVariantId, i)

                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                addtocart?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                            requireActivity().finish()
                            requireActivity().finishAffinity()
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
                else -> {}
            }
        }
    }

    private fun setCount(count: Int?) {
        _binding.tvCount.text = count.toString()
    }


    private fun dlgConfirm(message: String?) {
        val confirmDialogBinding = DlgConfirmBinding.inflate(layoutInflater)
        val dialog = Dialog(requireActivity())
        dialog.setContentView(confirmDialogBinding.root)
        confirmDialogBinding.tvProductDesc.text = message
        confirmDialogBinding.tvCheckOut.setOnClickListener {
            dialog.dismiss()
            openActivity(ActShoppingCart::class.java)
        }
        confirmDialogBinding.tvContinueShopping.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun dlgAlreadyCart(message: String?, id: Int?, defaultVariantId: Int?, i: Int?) {
        val builder = AlertDialog.Builder(requireActivity())
        builder.setTitle(R.string.app_name)
        builder.setMessage(R.string.product_already_again)
        builder.setPositiveButton(getString(R.string.yes)) { dialogInterface, which ->
            val cartqty = java.util.HashMap<String, String>()

            if (Utils.isLogin(requireActivity())) {
                count = 1
                cartqty["product_id"] = id.toString()
                cartqty["user_id"] =
                    SharePreference.getStringPref(
                        requireActivity(),
                        SharePreference.userId
                    ).toString()
                cartqty["variant_id"] = defaultVariantId.toString()
                cartqty["quantity_type"] = "increase"
                cartqty["theme_id"] = getString(R.string.theme_id)
                cartQtyApi(cartqty, "increase")
            } else {
                val cartListData = SharePreference.getStringPref(
                    requireActivity(),
                    SharePreference.GuestCartList
                )
                val type: Type = object : TypeToken<java.util.ArrayList<ProductListItem>>() {}.type

                var cartList =
                    Gson().fromJson<java.util.ArrayList<ProductListItem>>(cartListData, type)
                var count = cartList[i!!].qty?.toInt()!!
                Log.e("Count", count.toString())
                cartList[i].qty = count.plus(1)

                manageOfflineData(cartList)
            }
        }
        builder.setNegativeButton(getString(R.string.no)) { dialogInterface, which ->
            builder.setCancelable(true)
        }
        val alertDialog: AlertDialog = builder.create()
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    private fun manageOfflineData(cartItemList: java.util.ArrayList<ProductListItem>) {

        SharePreference.setStringPref(
            requireActivity(),
            SharePreference.GuestCartList,
            Gson().toJson(cartItemList)
        )
        SharePreference.setStringPref(
            requireActivity(),
            SharePreference.cartCount,
            cartItemList.size.toString()
        )
    }

    private fun cartQtyApi(
        cartqty: java.util.HashMap<String, String>,
        type: String,
    ) {
        Utils.showLoadingProgress(requireActivity())
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .cartQty(cartqty)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val cartQtyResponse = response.body.data
                    SharePreference.setStringPref(
                        requireActivity(),
                        SharePreference.cartCount,
                        response.body.count.toString()
                    )

                    when (response.body.status) {
                        1 -> {
                            Utils.dismissLoadingProgress()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                cartQtyResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                cartQtyResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()

                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(), response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(requireActivity(), "Something went wrong")
                }
                else -> {}
            }
        }
    }

    override fun onResume() {
        super.onResume()



        if (Utils.isCheckNetwork(requireActivity())) {
            lifecycleScope.launch {
                _binding.view.show()

                coroutineScope {


                    val currency = lifecycleScope.async { callCurrencyApi() }
                    currency.await()
                    val randomProduct = lifecycleScope.async { callRandomProduct() }
                    randomProduct.await()

                    callHeaderContentApi()

                    currentPageCategorirs = 1
                    homeCategoriesList.clear()
                    categoriesAdapter(homeCategoriesList)
                    val categories = lifecycleScope.async { callCategories() }
                    categories.await()


                    currentPageBestSeller = 1
                    bestsellersList.clear()
                    bestsellerAdapter(bestsellersList)

                    val bestSeller = lifecycleScope.async { callBestseller() }
                    bestSeller.await()
                    currentPageTrending = 1
                    trendingProductList.clear()
                    trendingProductAdapter(trendingProductList)

                    val trendingProduct = lifecycleScope.async {
                        callTrendingProducts()
                    }
                    trendingProduct.await()
                    _binding.view.hide()
                    _binding.tvCount.text =
                        SharePreference.getStringPref(requireActivity(), SharePreference.cartCount)

                }
            }
        } else {
            Utils.errorAlert(
                requireActivity(),
                resources.getString(R.string.internet_connection_error)
            )
        }


    }


    override fun onStop() {
        super.onStop()
        handler.removeCallbacks(autoScroll)

    }

}