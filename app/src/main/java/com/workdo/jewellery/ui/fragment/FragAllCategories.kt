package com.workdo.jewellery.ui.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.util.Log
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.workdo.jewellery.R
import com.workdo.jewellery.adapter.AllCategoryAdapter
import com.workdo.jewellery.api.ApiClient
import com.workdo.jewellery.base.BaseFragment
import com.workdo.jewellery.databinding.FragAllcategoriesBinding
import com.workdo.jewellery.model.HomeCategoriesItem
import com.workdo.jewellery.remote.NetworkResponse
import com.workdo.jewellery.ui.activity.ActCategoryProduct
import com.workdo.jewellery.ui.activity.ActShoppingCart
import com.workdo.jewellery.ui.authentication.ActWelCome
import com.workdo.jewellery.utils.Constants
import com.workdo.jewellery.utils.ExtensionFunctions.hide
import com.workdo.jewellery.utils.ExtensionFunctions.show
import com.workdo.jewellery.utils.PaginationScrollListener
import com.workdo.jewellery.utils.SharePreference
import com.workdo.jewellery.utils.Utils
import kotlinx.coroutines.launch

class FragAllCategories : BaseFragment<FragAllcategoriesBinding>() {

    private lateinit var _binding: FragAllcategoriesBinding
    private var managerAllCategories: GridLayoutManager? = null
    private var homeCategoriesList = ArrayList<HomeCategoriesItem>()
    private lateinit var allCategoriesAdapter: AllCategoryAdapter
    internal var isLoadingCategorirs = false
    internal var isLastPageCategorirs = false

    private var currentPageCategorirs = 1
    private var total_pagesCategorirs: Int = 0
    var arraysize = ""

    override fun initView(view: View) {

        init()

    }

    override fun getBinding(): FragAllcategoriesBinding {
        _binding = FragAllcategoriesBinding.inflate(layoutInflater)
        return _binding
    }

    companion object {

    }
    fun showLayout() {
        if (_binding.clRound.visibility==View.GONE)
        {
            _binding.clRound.visibility=View.VISIBLE
        }
    }
    @SuppressLint("NotifyDataSetChanged")
    private fun init() {
        paginationCategories()
        managerAllCategories = GridLayoutManager(requireActivity(), 1, GridLayoutManager.VERTICAL, false)
        _binding.clRound.show()
        _binding.tvCount.text = SharePreference.getStringPref(requireActivity(), SharePreference.cartCount).toString()
        _binding.clcart.setOnClickListener { openActivity(ActShoppingCart::class.java) }
        _binding.scrollView.viewTreeObserver?.addOnScrollChangedListener {
            val view = _binding.scrollView.getChildAt(_binding.scrollView.childCount - 1)
            val diff = view.bottom - (_binding.scrollView.height + _binding.scrollView.scrollY)
            if (diff == 0) {
                _binding.clRound.hide()
            }
        }
    }

    //TODO adapter set Categorirs
    private fun categoriesAdapter(homeCategoriesList: ArrayList<HomeCategoriesItem>) {
        _binding.rvAllCategories.layoutManager = managerAllCategories
        allCategoriesAdapter =
            AllCategoryAdapter(requireActivity(), homeCategoriesList) { i: Int, s: String ->
                if (s == Constants.ItemClick) {
                    startActivity(Intent(requireActivity(), ActCategoryProduct::class.java).putExtra("maincategory_id", homeCategoriesList[i].categoryId.toString()))
                }
            }
        _binding.rvAllCategories.adapter = allCategoriesAdapter
    }

    //TODO All Categories pagination
    private fun paginationCategories() {

        val paginationListener = object : PaginationScrollListener(managerAllCategories) {
            override fun isLastPage(): Boolean {
                return isLastPageCategorirs
            }

            override fun isLoading(): Boolean {
                return isLoadingCategorirs
            }

            override fun loadMoreItems() {
                isLoadingCategorirs = true
                currentPageCategorirs++
                callCategories()
            }
        }
        _binding.rvAllCategories.addOnScrollListener(paginationListener)
    }


    //TODO  Categories api callig
    private fun callCategories() {
        Utils.showLoadingProgress(requireActivity())
        val categorieshashmap = HashMap<String, String>()
        categorieshashmap["theme_id"] =getString(R.string.theme_id)
        lifecycleScope.launch {
            when (val response = ApiClient.getClient(requireActivity())
                .setHomeCategorys(currentPageCategorirs.toString(),categorieshashmap)) {
                is NetworkResponse.Success -> {
                    Utils.dismissLoadingProgress()
                    val categoriesResponse = response.body.data
                    when (response.body.status) {
                        1 -> {
                            if ((response.body.data?.data?.size ?: 0) > 0) {
                                arraysize = categoriesResponse?.data?.size.toString()
                                setRoundCat(categoriesResponse?.data, arraysize)
                                Log.e("LISTSZIE", arraysize)
                                _binding.rvAllCategories.show()
                                _binding.clRound.show()
                                currentPageCategorirs = categoriesResponse?.currentPage?.toInt()?:0
                                total_pagesCategorirs = categoriesResponse?.lastPage?.toInt()?:0
                                categoriesResponse?.data?.let {
                                    homeCategoriesList.addAll(it)
                                }
                                if (currentPageCategorirs >= total_pagesCategorirs) {
                                    isLastPageCategorirs = true
                                }
                                isLoadingCategorirs = false
                            } else {
                                _binding.rvAllCategories.hide()
                                _binding.clRound.hide()
                            }
                            allCategoriesAdapter.notifyDataSetChanged()
                        }
                        0 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                categoriesResponse?.message.toString()
                            )
                        }
                        9 -> {
                            Utils.errorAlert(
                                requireActivity(),
                                categoriesResponse?.message.toString()
                            )
                            openActivity(ActWelCome::class.java)
                        }
                    }
                }

                is NetworkResponse.ApiError -> {
                    Utils.dismissLoadingProgress()
                    if (response.body.status == 9) {
                        Utils.setInvalidToekn(requireActivity())
                    } else {
                        Utils.errorAlert(
                            requireActivity(),
                            response.body.message.toString()
                        )
                    }
                }

                is NetworkResponse.NetworkError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        resources.getString(R.string.internet_connection_error)
                    )
                }

                is NetworkResponse.UnknownError -> {
                    Utils.dismissLoadingProgress()
                    Utils.errorAlert(
                        requireActivity(),
                        "Something went wrong"
                    )
                }
            }
        }
    }

    //TODO set round menu
    private fun setRoundCat(data: ArrayList<HomeCategoriesItem>?, arraysize: String) {
        if (arraysize == "1") {
            _binding.clCat1.hide()
            _binding.clCat2.hide()
            _binding.clCat3.hide()
            _binding.clCat4.hide()
            _binding.clCat5.show()
            Glide.with(requireActivity())
                .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(0)?.iconPath.toString()))
                .into(_binding.ivCat5)
            _binding.tvCat5.text = data?.get(0)?.name

            _binding.clCat5.setOnClickListener {
                startActivity(
                    Intent(
                        requireActivity(),
                        ActCategoryProduct::class.java
                    ).putExtra("maincategory_id", data?.get(0)?.categoryId.toString())
                )
            }
        } else if (arraysize == "2") {
            _binding.clCat1.show()
            _binding.clCat2.hide()
            _binding.clCat3.hide()
            _binding.clCat4.show()
            _binding.clCat5.hide()
            Glide.with(requireActivity())
                .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(0)?.iconPath.toString()))
                .into(_binding.ivCat1)
            _binding.tvCat1.text = data?.get(0)?.name

            Glide.with(requireActivity())
                .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(1)?.iconPath.toString()))
                .into(_binding.ivCat4)
            _binding.tvCat4.text = data?.get(1)?.name

            _binding.clCat1.setOnClickListener {
                startActivity(
                    Intent(
                        requireActivity(),
                        ActCategoryProduct::class.java
                    ).putExtra("maincategory_id", data?.get(0)?.categoryId.toString())
                )
            }
            _binding.clCat4.setOnClickListener {
                startActivity(
                    Intent(
                        requireActivity(),
                        ActCategoryProduct::class.java
                    ).putExtra("maincategory_id", data?.get(1)?.categoryId.toString())
                )
            }
        } else if (arraysize == "3") {
            _binding.clCat1.show()
            _binding.clCat2.hide()
            _binding.clCat3.hide()
            _binding.clCat4.show()
            _binding.clCat5.show()
            Glide.with(requireActivity())
                .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(0)?.iconPath.toString()))
                .into(_binding.ivCat1)
            _binding.tvCat1.text = data?.get(0)?.name

            Glide.with(requireActivity())
                .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(1)?.iconPath.toString()))
                .into(_binding.ivCat5)
            _binding.tvCat5.text = data?.get(1)?.name

            Glide.with(requireActivity())
                .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(2)?.iconPath.toString()))
                .into(_binding.ivCat4)
            _binding.tvCat4.text = data?.get(2)?.name

            _binding.clCat1.setOnClickListener {
                startActivity(
                    Intent(
                        requireActivity(),
                        ActCategoryProduct::class.java
                    ).putExtra("maincategory_id", data?.get(0)?.categoryId.toString())
                )
            }
            _binding.clCat5.setOnClickListener {
                startActivity(
                    Intent(
                        requireActivity(),
                        ActCategoryProduct::class.java
                    ).putExtra("maincategory_id", data?.get(1)?.categoryId.toString())
                )
            }
            _binding.clCat4.setOnClickListener {
                startActivity(
                    Intent(
                        requireActivity(),
                        ActCategoryProduct::class.java
                    ).putExtra("maincategory_id", data?.get(2)?.categoryId.toString())
                )
            }
        } else if (arraysize == "4") {
            _binding.clCat1.show()
            _binding.clCat2.show()
            _binding.clCat3.show()
            _binding.clCat4.show()
            _binding.clCat5.hide()
            Glide.with(requireActivity())
                .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(0)?.iconPath.toString()))
                .into(_binding.ivCat1)
            _binding.tvCat1.text = data?.get(0)?.name

            Glide.with(requireActivity())
                .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(1)?.iconPath.toString()))
                .into(_binding.ivCat2)
            _binding.tvCat2.text = data?.get(1)?.name

            Glide.with(requireActivity())
                .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(2)?.iconPath.toString()))
                .into(_binding.ivCat3)
            _binding.tvCat3.text = data?.get(2)?.name

            Glide.with(requireActivity())
                .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(3)?.iconPath.toString()))
                .into(_binding.ivCat4)
            _binding.tvCat4.text = data?.get(3)?.name

            _binding.clCat1.setOnClickListener {
                startActivity(
                    Intent(
                        requireActivity(),
                        ActCategoryProduct::class.java
                    ).putExtra("maincategory_id", data?.get(0)?.categoryId.toString())
                )
            }
            _binding.clCat2.setOnClickListener {
                startActivity(
                    Intent(
                        requireActivity(),
                        ActCategoryProduct::class.java
                    ).putExtra("maincategory_id", data?.get(1)?.categoryId.toString())
                )
            }
            _binding.clCat3.setOnClickListener {
                startActivity(
                    Intent(
                        requireActivity(),
                        ActCategoryProduct::class.java
                    ).putExtra("maincategory_id", data?.get(2)?.categoryId.toString())
                )
            }
            _binding.clCat4.setOnClickListener {
                startActivity(
                    Intent(
                        requireActivity(),
                        ActCategoryProduct::class.java
                    ).putExtra("maincategory_id", data?.get(3)?.categoryId.toString())
                )
            }
        } else {
            _binding.clCat1.show()
            _binding.clCat2.show()
            _binding.clCat3.show()
            _binding.clCat4.show()
            _binding.clCat5.hide()
            Glide.with(requireActivity())
                .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(0)?.iconPath.toString()))
                .into(_binding.ivCat1)
            _binding.tvCat1.text = data?.get(0)?.name

            Glide.with(requireActivity())
                .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(1)?.iconPath.toString()))
                .into(_binding.ivCat2)
            _binding.tvCat2.text = data?.get(1)?.name

            Glide.with(requireActivity())
                .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(2)?.iconPath.toString()))
                .into(_binding.ivCat3)
            _binding.tvCat3.text = data?.get(2)?.name

            Glide.with(requireActivity())
                .load(ApiClient.ImageURL.BASE_URL.plus(data?.get(3)?.iconPath.toString()))
                .into(_binding.ivCat4)
            _binding.tvCat4.text = data?.get(3)?.name
            _binding.clCat1.setOnClickListener {
                startActivity(
                    Intent(
                        requireActivity(),
                        ActCategoryProduct::class.java
                    ).putExtra("maincategory_id", data?.get(0)?.categoryId.toString())
                )
            }
            _binding.clCat2.setOnClickListener {
                startActivity(
                    Intent(
                        requireActivity(),
                        ActCategoryProduct::class.java
                    ).putExtra("maincategory_id", data?.get(1)?.categoryId.toString())
                )
            }
            _binding.clCat3.setOnClickListener {
                startActivity(
                    Intent(
                        requireActivity(),
                        ActCategoryProduct::class.java
                    ).putExtra("maincategory_id", data?.get(2)?.categoryId.toString())
                )
            }
            _binding.clCat4.setOnClickListener {
                startActivity(
                    Intent(
                        requireActivity(),
                        ActCategoryProduct::class.java
                    ).putExtra("maincategory_id", data?.get(3)?.categoryId.toString())
                )
            }
        }
    }

    override fun onResume() {
        super.onResume()
        currentPageCategorirs = 1
        homeCategoriesList.clear()
        categoriesAdapter(homeCategoriesList)
        callCategories()
        _binding.tvCount.text =
            SharePreference.getStringPref(requireActivity(), SharePreference.cartCount).toString()
    }

}