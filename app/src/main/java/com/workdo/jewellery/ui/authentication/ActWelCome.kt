package com.workdo.jewellery.ui.authentication

import android.content.Intent
import android.view.View
import com.workdo.jewellery.base.BaseActivity
import com.workdo.jewellery.databinding.ActWelComeBinding
import com.workdo.jewellery.ui.activity.MainActivity

class ActWelCome : BaseActivity() {
    private lateinit var _binding: ActWelComeBinding

    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActWelComeBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        _binding.btnLogin.setOnClickListener {
            openActivity(ActLoginOption::class.java)
        }
        _binding.btnSignup.setOnClickListener {
            openActivity(ActRegisterOption::class.java)
        }
        _binding.btnGuest.setOnClickListener {
            startActivity(
                Intent(
                    this@ActWelCome,
                    MainActivity::class.java
                )
            )
            finish()
        }
    }

}