package com.workdo.jewellery.ui.authentication

import android.view.View
import com.workdo.jewellery.base.BaseActivity
import com.workdo.jewellery.databinding.ActSuccessfullBinding
import com.workdo.jewellery.ui.activity.MainActivity

class ActSuccessfull : BaseActivity() {
    private lateinit var _binding: ActSuccessfullBinding
    override fun setLayout(): View = _binding.root

    override fun initView() {
        _binding = ActSuccessfullBinding.inflate(layoutInflater)
        init()
    }

    private fun init() {
        _binding.btnGotoyourstore.setOnClickListener { openActivity(MainActivity::class.java) }
    }
}
