package com.workdo.jewellery.utils;

import com.workdo.jewellery.model.CityListData;

public interface OnItemClickListenerGuestCity {
    void onItemClickCityGuest(CityListData item);
}
