package com.workdo.jewellery.utils;

import com.workdo.jewellery.model.StateListData;

public interface OnItemClickListenerGuestState {
    void onItemClickStateGuest(StateListData item);
}
