package com.workdo.jewellery.utils;

import com.workdo.jewellery.model.CountryDataItem;

public interface OnItemClickListenerCountry {
    void onItemClick(CountryDataItem item);
}
