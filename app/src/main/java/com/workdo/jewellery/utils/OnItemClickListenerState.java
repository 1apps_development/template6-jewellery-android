package com.workdo.jewellery.utils;

import com.workdo.jewellery.model.StateListData;

public interface OnItemClickListenerState {
    void onItemClickState(StateListData item);
}
