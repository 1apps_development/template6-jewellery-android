package com.workdo.jewellery.utils;

import com.workdo.jewellery.model.CityListData;

public interface OnItemClickListenerCity {
    void onItemClickCity(CityListData item);
}
