package com.workdo.jewellery.utils;

import com.workdo.jewellery.model.CountryDataItem;

public interface OnItemClickListenerGuestCountry {
    void onItemClickGuest(CountryDataItem item);
}
