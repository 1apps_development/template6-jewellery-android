package com.workdo.jewellery.model

import com.google.gson.annotations.SerializedName

data class TaxGuestResponse(

	@field:SerializedName("data")
	val data: TaxGuestData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)



data class TaxGuestData(

	@field:SerializedName("original_price")
	val originalPrice: String? = null,

	@field:SerializedName("tax_info")
	val taxInfo: ArrayList<TaxItem>? = null,

	@field:SerializedName("final_price")
	val finalPrice: String? = null,

	@field:SerializedName("total_tax_price")
	val totalTaxPrice: String? = null
)
