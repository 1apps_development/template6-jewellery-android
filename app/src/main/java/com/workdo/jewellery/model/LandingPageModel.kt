package com.workdo.jewellery.model

import com.google.gson.annotations.SerializedName

data class LandingPageModel(

	@field:SerializedName("data")
	val data: LandingPageData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class HomepageProducts(

	@field:SerializedName("homepage-products-bg-img")
	val homepageProductsBgImg: List<String?>? = null,

	@field:SerializedName("homepage-products-btn")
	val homepageProductsBtn: String? = null,

	@field:SerializedName("homepage-products-btn-icon")
	val homepageProductsBtnIcon: List<Any?>? = null,

	@field:SerializedName("homepage-products-label")
	val homepageProductsLabel: String? = null,

	@field:SerializedName("homepage-products-title")
	val homepageProductsTitle: String? = null
)

data class HomepagePromotions(

	@field:SerializedName("homepage-promotions-promotion-icon")
	val homepagePromotionsPromotionIcon: List<String?>? = null,

	@field:SerializedName("homepage-promotions-title")
	val homepagePromotionsTitle: String? = null
)

data class ThemJson(

	@field:SerializedName("homepage-header")
	val homepageHeader: HomepageHeader? = null,

	@field:SerializedName("homepage-newsletter")
	val homepageNewsletter: HomepageNewsletter? = null,

	@field:SerializedName("homepage-products")
	val homepageProducts: HomepageProducts? = null,

	@field:SerializedName("homepage-banner")
	val homepageBanner: HomepageBanner? = null,

	@field:SerializedName("homepage-promotions")
	val homepagePromotions: HomepagePromotions? = null,

	@field:SerializedName("homepage-best-product")
	val homepageBestProduct: HomepageBestProduct? = null
)

data class HomepageNewsletter(

	@field:SerializedName("homepage-newsletter-sub-text")
	val homepageNewsletterSubText: String? = null,

	@field:SerializedName("homepage-newsletter-title")
	val homepageNewsletterTitle: String? = null,

	@field:SerializedName("homepage-newsletter-description")
	val homepageNewsletterDescription: String? = null
)

data class HomepageBestProduct(

	@field:SerializedName("homepage-best-product-sub-text")
	val homepageBestProductSubText: String? = null,

	@field:SerializedName("homepage-best-product-title")
	val homepageBestProductTitle: String? = null
)

data class HomepageHeader(

	@field:SerializedName("homepage-header-bg-img")
	val homepageHeaderBgImg: List<String?>? = null,

	@field:SerializedName("homepage-header-btn")
	val homepageHeaderBtn: String? = null,

	@field:SerializedName("homepage-header-btn-icon")
	val homepageHeaderBtnIcon: List<Any?>? = null,

	@field:SerializedName("homepage-header-title")
	val homepageHeaderTitle: String? = null,

	@field:SerializedName("homepage-header-sub-text")
	val homepageHeaderSubText: String? = null
)

data class LandingPageData(

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("them_json")
	val themJson: ThemJson? = null,

	@field:SerializedName("loyality_section")
	val loyalitySection: String? = null
)

data class HomepageBanner(

	@field:SerializedName("homepage-banner-img")
	val homepageBannerImg: List<String?>? = null,

	@field:SerializedName("homepage-banner-title")
	val homepageBannerTitle: String? = null,

	@field:SerializedName("homepage-banner-sub-text")
	val homepageBannerSubText: String? = null
)
